# Vault CLI wrapper crate

## Getting started

Define environment variables before usage:
- `VAULT_ADDR`
- `VAULT_TOKEN`

Then use:

```rust
let executor = DefaultVaultCliExecutor::new();

let wrapper = DefaultVaultCliWrapper::new(&executor);

# Read secrets as key-value
let secrets = wrapper.read_secrets_data("kv/data/demo/app").unwrap();
```

## Safety

Crate has safe mode by default, don't log any sensitive data.