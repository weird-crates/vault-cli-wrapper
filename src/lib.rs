use std::collections::HashMap;
use log::{debug, error, info};
use crate::error::VaultCliWrapperError;
use crate::executor::{VaultCliExecutor};

pub mod executor;
pub mod error;

pub trait VaultCliWrapper {
    /// vault read -field=data -format=json kv/data/demo/app
    fn read_secrets_data(&self, vault_path: &str) -> Result<HashMap<String,String>, VaultCliWrapperError>;
}

pub struct DefaultVaultCliWrapper<'a> {
    pub executor: &'a dyn VaultCliExecutor,
    pub safe_mode: bool
}

impl DefaultVaultCliWrapper<'_> {
    pub fn new(executor: &dyn VaultCliExecutor) -> DefaultVaultCliWrapper<'_> {
        DefaultVaultCliWrapper {
            executor,
            safe_mode: true,
        }
    }
}

impl VaultCliWrapper for DefaultVaultCliWrapper<'_> {
    /// vault read -field=data -format=json kv/data/demo/app
    fn read_secrets_data(&self, vault_path: &str) -> Result<HashMap<String, String>, VaultCliWrapperError> {

        let output = &self.executor.execute(&format!("read -field=data -format=json {vault_path}"))?;
        let output = output.trim_matches('\n');

        match serde_json::from_str::<HashMap<String,String>>(&output) {
            Ok(results) => {
                if !&self.safe_mode {
                    debug!("secret values:");
                    debug!("{:?}", results);
                }

                info!("secrets have been read from vault");

                Ok(results)
            }
            Err(e) => {
                error!("vault cli data deserialization error: {}", e);
                Err(VaultCliWrapperError::DeserializationError(e))
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use std::collections::HashMap;
    use std::fs;
    use std::path::Path;
    use crate::{DefaultVaultCliWrapper, VaultCliWrapper};
    use crate::error::VaultCliWrapperError;
    use crate::executor::VaultCliExecutor;

    struct MockVaultCliExecutor {
        pub output: String
    }

    impl MockVaultCliExecutor {
        pub fn new(output: &str) -> Self {
            Self {
                output: output.to_string()
            }
        }
    }

    impl VaultCliExecutor for MockVaultCliExecutor {
        fn execute(&self, _args: &str) -> Result<String, VaultCliWrapperError> {
            Ok(self.output.to_string())
        }
    }

    #[test]
    fn return_secrets() {
        let output_file = Path::new("test-data").join("output.json");
        let output = fs::read_to_string(&output_file).unwrap();

        let executor = MockVaultCliExecutor::new(&output);

        let wrapper = DefaultVaultCliWrapper::new(&executor);

        let secrets = wrapper.read_secrets_data("whatever").unwrap();

        let expected_secrets: HashMap<String,String> = HashMap::from([
            ("API_AUTH_LOGIN".to_string(), "dinosaur".to_string()),
            ("API_AUTH_PASSWORD".to_string(), "g2059g34A_1294Dj".to_string())
        ]);

        for (expected_key, expected_value) in expected_secrets {
            assert!(&secrets.contains_key(&expected_key));

            let value = secrets.get(&expected_key).unwrap();
            assert_eq!(value, &expected_value)
        }
    }
}