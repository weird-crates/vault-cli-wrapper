use std::process::Command;
use log::{debug, error, info};
use crate::error::VaultCliWrapperError;

pub trait VaultCliExecutor {
    fn execute(&self, args: &str) -> Result<String,VaultCliWrapperError>;
}

pub struct DefaultVaultCliExecutor {
    pub safe_mode: bool
}

impl DefaultVaultCliExecutor {
    pub fn new() -> Self {
        Self {
            safe_mode: true,
        }
    }

    /// Log sensitive data
    pub fn new_unsafe() -> Self {
        Self {
            safe_mode: false,
        }
    }
}

impl VaultCliExecutor for DefaultVaultCliExecutor {
    fn execute(&self, args: &str) -> Result<String, VaultCliWrapperError> {
        info!("execute vault cli command with args '{args}'..");

        let args: Vec<&str> = args.split(" ").collect();

        match Command::new("vault").args(args).output() {
            Ok(output) => {
                if output.status.success() {
                    let stdout = String::from_utf8(output.stdout)?;

                    let stdout = stdout.trim_matches('\'');

                    if !&self.safe_mode {
                        debug!("<stdout>");
                        debug!("{}", stdout);
                        debug!("</stdout>");
                    }

                    Ok(stdout.to_string())

                } else {
                    error!("command execution error");
                    let stderr = String::from_utf8_lossy(&output.stderr);

                    error!("<stderr>");
                    error!("{}", stderr);
                    error!("</stderr>");

                    Err(VaultCliWrapperError::Error)
                }
            }
            Err(e) => {
                error!("execution error: {}", e);
                Err(VaultCliWrapperError::ExecutionError(e))
            }
        }
    }
}
